package com.ericjohnson.myroomdatabase.view

import com.ericjohnson.myroomdatabase.data.model.Contact


interface GetContactCallback {

    fun getContact(contactList: List<Contact>)
}