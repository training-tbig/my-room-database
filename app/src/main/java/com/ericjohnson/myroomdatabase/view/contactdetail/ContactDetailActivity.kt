package com.ericjohnson.myroomdatabase.view.contactdetail

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.ericjohnson.myroomdatabase.R.layout
import com.ericjohnson.myroomdatabase.data.model.Contact
import com.ericjohnson.myroomdatabase.data.repository.ContactRepository
import com.ericjohnson.myroomdatabase.view.GetContactCallback
import kotlinx.android.synthetic.main.activity_contact_detail.btnSave
import kotlinx.android.synthetic.main.activity_contact_detail.etEmail
import kotlinx.android.synthetic.main.activity_contact_detail.etName

class ContactDetailActivity : AppCompatActivity(), GetContactCallback {

    companion object {
        const val KEY_CONTACT_ID = "key_contact_id"

        const val KEY_EDIT_CONTACT = "key_edit_contact"
    }

    private lateinit var contactRepository: ContactRepository

    private var contactId = 0

    private var isEdit = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_contact_detail)
        contactRepository = ContactRepository(application)

        contactId = intent.getIntExtra(KEY_CONTACT_ID, 0)
        isEdit = intent.getBooleanExtra(KEY_EDIT_CONTACT, false)

        if (isEdit) {
            contactRepository.getSingleContact(this, contactId)
        }

        btnSave.setOnClickListener {
            val name = etName.text.trim().toString()
            val email = etEmail.text.trim().toString()
            if (isEdit) {
                contactRepository.updateContact(Contact(contactId, name, email))
            } else {
                contactRepository.insertContact(Contact(name = name, email = email))
            }
            Toast.makeText(this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show()
            finish()
        }
    }

    override fun getContact(contactList: List<Contact>) {
        val contact = contactList[0]

        etName.setText(contact.name)
        etEmail.setText(contact.email)
    }
}
