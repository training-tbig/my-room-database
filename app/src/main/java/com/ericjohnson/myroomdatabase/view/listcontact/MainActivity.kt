package com.ericjohnson.myroomdatabase.view.listcontact

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ericjohnson.myroomdatabase.R.layout
import com.ericjohnson.myroomdatabase.view.listcontact.ContactAdapter.OnItemClickListener
import com.ericjohnson.myroomdatabase.data.model.Contact
import com.ericjohnson.myroomdatabase.data.repository.ContactRepository
import com.ericjohnson.myroomdatabase.view.contactdetail.ContactDetailActivity
import com.ericjohnson.myroomdatabase.view.GetContactCallback
import kotlinx.android.synthetic.main.activity_main.fabContact
import kotlinx.android.synthetic.main.activity_main.rvContact

class MainActivity : AppCompatActivity(), OnItemClickListener,
    GetContactCallback {

    private lateinit var contactRepository: ContactRepository

    private lateinit var contactAdapter: ContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)
        contactRepository = ContactRepository(application)
        contactAdapter = ContactAdapter(this, this)

        rvContact.layoutManager = LinearLayoutManager(this)
        rvContact.setHasFixedSize(true)
        rvContact.addItemDecoration(DividerItemDecoration(this, RecyclerView.VERTICAL))
        rvContact.adapter = contactAdapter

        fabContact.setOnClickListener {
            startActivity(Intent(this, ContactDetailActivity::class.java))
        }
    }

    override fun onResume() {
        super.onResume()
        getAllContacts()
    }

    override fun onItemClick(item: Contact) {

        val intent = Intent(this, ContactDetailActivity::class.java)
        intent.putExtra(ContactDetailActivity.KEY_CONTACT_ID, item.id)
        intent.putExtra(ContactDetailActivity.KEY_EDIT_CONTACT, true)
        startActivity(intent)
    }

    override fun onLongItemClicked(item: Contact) {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Peringatan")
            .setMessage("Hapus kontak?")
            .setNegativeButton(
                "Cancel"
            ) { dialoginterface, _ -> dialoginterface.cancel() }
            .setPositiveButton("Ok") { dialoginterface, i ->
                contactRepository.deleteContact(item)
                dialoginterface.dismiss()
                getAllContacts()
            }.show()
    }

    private fun getAllContacts() {
        contactRepository.getAllContact(this)
    }

    override fun getContact(contactList: List<Contact>) {
        contactAdapter.items.clear()
        when {
            contactList.isEmpty() -> rvContact.visibility = View.GONE
            else -> {
                rvContact.visibility = View.VISIBLE
                contactAdapter.items.addAll(contactList)
            }
        }
        contactAdapter.notifyDataSetChanged()
    }
}
