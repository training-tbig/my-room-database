package com.ericjohnson.myroomdatabase.view


enum class DbActionType(val type: Int) {

    SELECT_ALL(0),
    SELECT(1),
    INSERT(2),
    UPDATE(3),
    DELETE(4)
}