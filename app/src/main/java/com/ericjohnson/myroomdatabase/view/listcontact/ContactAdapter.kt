package com.ericjohnson.myroomdatabase.view.listcontact

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ericjohnson.myroomdatabase.R.layout
import com.ericjohnson.myroomdatabase.data.model.Contact
import com.ericjohnson.myroomdatabase.view.listcontact.ContactAdapter.ViewHolder
import kotlinx.android.synthetic.main.item_contact.view.contactItem
import kotlinx.android.synthetic.main.item_contact.view.tvEmail
import kotlinx.android.synthetic.main.item_contact.view.tvName

/**
 * Created by johnson on 7/25/19.
 */
class ContactAdapter(
    private val context: Context,
    private val onItemClickListener: OnItemClickListener? = null
) : RecyclerView.Adapter<ViewHolder>() {

    var items = mutableListOf<Contact>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(context)
            .inflate(layout.item_contact, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Contact) {
            with(itemView) {
                tvName.text = "Nama: ${item.name}"
                tvEmail.text = "Email: ${item.email}"

                contactItem.setOnClickListener {
                    onItemClickListener?.onItemClick(item)
                }

                contactItem.setOnLongClickListener {
                    onItemClickListener?.onLongItemClicked(item)
                    true
                }

            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(item: Contact)

        fun onLongItemClicked(item: Contact)
    }
}
