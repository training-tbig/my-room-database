package com.ericjohnson.myroomdatabase.data.repository

import android.app.Application
import android.os.AsyncTask
import com.ericjohnson.myroomdatabase.view.DbActionType
import com.ericjohnson.myroomdatabase.view.GetContactCallback
import com.ericjohnson.myroomdatabase.data.AppDatabase
import com.ericjohnson.myroomdatabase.data.dao.ContactDao
import com.ericjohnson.myroomdatabase.data.model.Contact

/**
 * Created by johnson on 7/25/19.
 */
class ContactRepository(application: Application) {

    companion object {
        private lateinit var contactDao: ContactDao
    }

    init {
        val db = AppDatabase.getDatabase(application)
        if (db != null) {
            contactDao = db.contractDao()
        }
    }

    fun insertContact(contact: Contact) {
        ContactQuery(DbActionType.INSERT.type).execute(contact)
    }

    fun updateContact(contact: Contact) {
        ContactQuery(DbActionType.UPDATE.type).execute(contact)
    }

    fun deleteContact(contact: Contact) {
        ContactQuery(DbActionType.DELETE.type).execute(contact)
    }

    fun getAllContact(getContactCallback: GetContactCallback) {
        GetContactQuery(DbActionType.SELECT_ALL.type, 0,
            getContactCallback).execute()
    }

    fun getSingleContact(getContactCallback: GetContactCallback, id: Int) {
        GetContactQuery(DbActionType.SELECT.type, id, getContactCallback).execute()
    }

    private class ContactQuery internal constructor(internal var actionType: Int) : AsyncTask<Contact, Void, Void>() {

        override fun doInBackground(vararg contacts: Contact): Void? {
            when (actionType) {
                DbActionType.INSERT.type -> contactDao.insertContact(contacts[0])

                DbActionType.UPDATE.type -> contactDao.updateContact(contacts[0])

                DbActionType.DELETE.type -> contactDao.deleteContact(contacts[0])
            }
            return null
        }
    }

    private class GetContactQuery internal constructor(
        internal var actionType: Int,
        internal var contactId: Int,
        internal var getContactCallback: GetContactCallback
    ) : AsyncTask<Void, Void, List<Contact>>() {

        override fun doInBackground(vararg voids: Void?): List<Contact> {
            return if (actionType == DbActionType.SELECT.type) {
                contactDao.getContact(contactId)
            } else {
                contactDao.getAllContact()
            }
        }

        override fun onPostExecute(result: List<Contact>) {
            super.onPostExecute(result)
            getContactCallback.getContact(result)
        }
    }
}