package com.ericjohnson.myroomdatabase.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.ericjohnson.myroomdatabase.data.model.Contact


@Dao
interface ContactDao {

    @Query("select * from contact_table ORDER BY id ASC")
    fun getAllContact(): List<Contact>

    @Query("select * from contact_table where id=:contactId")
    fun getContact(contactId: Int): List<Contact>

    @Insert
    fun insertContact(contact: Contact)

    @Update
    fun updateContact(contact: Contact)

    @Delete
    fun deleteContact(contact: Contact)
}