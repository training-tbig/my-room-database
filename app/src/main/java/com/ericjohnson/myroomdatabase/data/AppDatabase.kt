package com.ericjohnson.myroomdatabase.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ericjohnson.myroomdatabase.data.dao.ContactDao
import com.ericjohnson.myroomdatabase.data.model.Contact


@Database(entities = [Contact::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun contractDao(): ContactDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        fun getDatabase(context: Context):AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext, AppDatabase::class.java,
                        "MyDatabase"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyDatabase(){
            INSTANCE = null
        }
    }
}